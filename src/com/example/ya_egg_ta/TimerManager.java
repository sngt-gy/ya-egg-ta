package com.example.ya_egg_ta;

import java.util.ArrayList;

/**
 * Created by sangeight on 27/02/2014.
 * A Singleton class to allow sharing
 * single instances of collections across classes
 * Stores and allows access arraylists of timers and threads
 * With helper functions
 */
public class TimerManager {
    private static TimerManager instance = null;
    private ArrayList<TimerModel> collection;
    private ArrayList<Thread> threads;

    public TimerManager() {
        this.collection = new ArrayList<TimerModel>();
        this.threads = new ArrayList<Thread>();
    }

    public static synchronized TimerManager getInstance() {
        if(instance == null) {
            instance = new TimerManager();
        }
        return instance;
    }

    public ArrayList<Thread> getThreads() { return threads; }

    public ArrayList<TimerModel> getCollection() { return collection; }

    public boolean checkTimerIndex(int index) {
        return (index >= 0 && index < collection.size());
    }

    public boolean checkThreadIndex(int index) {
        return (index >= 0 && index < threads.size());
    }


}
