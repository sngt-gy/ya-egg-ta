package com.example.ya_egg_ta;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by sangeight on 27/02/2014.
 * Class to run the timer on a thread of its own
 * Sends broadcast signal everytime a timer value is updated
 */
public class TimerRunner implements Runnable {
    private static final String TAG = "YET: Timer-Runner";

    private final int DELAY = 1000;

    private TimerManager timerManager;
    private TimerService timerService;
    private TimerModel timer;
    private Intent intent;

    public TimerRunner(TimerModel timer, TimerService service) {
        this.timer = timer;
        this.timerService = service;

        timerManager = TimerManager.getInstance();
        intent = new Intent("timer-update");
    }

    @Override
    public void run() {
        while (timer.getTime() > 0) {
            if (timer.isActive()) {
                try {
                    Thread.sleep(DELAY);
                    timer.countDown();
                    int index = timerManager.getCollection().indexOf(timer);

                    /*
                     put extra data contain the index of the timer for
                     to able to filter the broadcast for individual timers if needed
                     */
                    if(timerManager.checkTimerIndex(index)) {
                        intent.putExtra("timer-update-index", index);
                    }
                    // send broadcast after every countdown to update timers
                    LocalBroadcastManager.getInstance(timerService)
                            .sendBroadcast(intent);

                    Log.d(TAG, "Tick Tock: " + timer.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // Notify that the timer has been completed
        timerService.notifyCompleted(timer);
    }

}
