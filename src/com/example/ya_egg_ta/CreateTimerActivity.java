package com.example.ya_egg_ta;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

/**
 * Created by sangeight on 11/03/2014.
 * Activity that holds the form for creating new timer.
 * Binds to TimerService to create new timer
 */
public class CreateTimerActivity extends Activity {
    private static final String TAG = "YET: Activity-CreateTimer";

    private boolean isBounded;
    private EditText textTitle;
    private NumberPicker pickerHours, pickerMinutes, pickerSeconds;
    private Button buttonSubmit;

    public TimerService timerService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.form_newtimer);

        // Initialisation
        isBounded = false;

        textTitle = (EditText) findViewById(R.id.create_timer_title);
        pickerHours = (NumberPicker) findViewById(R.id.create_timer_hours);
        pickerMinutes = (NumberPicker) findViewById(R.id.create_timer_minutes);
        pickerSeconds = (NumberPicker) findViewById(R.id.create_timer_seconds);
        buttonSubmit = (Button) findViewById(R.id.btnCreateTimerSubmit);

        // Set the ranges for the pickers
        pickerHours.setMinValue(0);
        pickerMinutes.setMinValue(0);
        pickerSeconds.setMinValue(0);

        pickerHours.setMaxValue(23);
        pickerMinutes.setMaxValue(59);
        pickerSeconds.setMaxValue(59);

        setUpListeners();
    }
    private void connectService() {
        // bind the timer service to the activity
        Intent intent = new Intent(CreateTimerActivity.this, TimerService.class);
        bindService(intent, conn, BIND_AUTO_CREATE);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Make sure we are bounded to the service
        // when we return to the application
        if(!isBounded) {
            connectService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        /*
         Make sure the activity is unbounded to the service
         when the activity stops.
         */
        if (isBounded) {
            unbindService(conn);
            isBounded = false;
        }
    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder server) {
            timerService = ((TimerService.LocalBinder) server).getInstance();
            isBounded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            timerService = null;
            isBounded = false;
        }
    };

    public void setUpListeners() {
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Clickedd");
                String title = textTitle.getText().toString();
                int hours = (int) pickerHours.getValue();
                int minutes = (int) pickerMinutes.getValue();
                int seconds = (int) pickerSeconds.getValue();
                if (isBounded) {
                    timerService.newTimer(title, hours, minutes, seconds);
                    startActivity(new Intent(CreateTimerActivity.this, HomeActivity.class));
                    // Make sure we destroy the activity
                    finish();
                }
            }
        });
    }
}
