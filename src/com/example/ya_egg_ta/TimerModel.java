package com.example.ya_egg_ta;

/**
 * Created by sangeight on 27/02/2014.
 * Timer Model, that stores the different associated values
 * Allows for pretty printing
 */
public class TimerModel {
    private String title;
    private int totalTime;
    private int time;
    private boolean isActive;
    private String timerCompletedText;

    public TimerModel(String title, int time, boolean isActive) {
        this.title = title;
        this.totalTime = this.time = time;
        this.timerCompletedText = "Completed";
        //Allows for
        this.isActive = isActive;
    }

    public int getTime() {
        return time; }

    public int getTotalTime() {
        return totalTime; }

    public String getTitle() {
        return title;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isCompleted() {
        return (time == 0);
    }

    public void countDown() {
        time--;
    }

    public void startTimer() {
        isActive = true;
    }

    public void stopTimer() {
        isActive = false;
    }

    public void resetTimer() {
        time = totalTime;
    }

    public void toggleTimer() {
        if (isActive) {
            isActive = false;
        } else {
            isActive = true;
        }
    }

    public String displayTime() {
        if(isCompleted()) {
            return timerCompletedText;
        }
        return getDisplayTime(time);
    }

    public String displayTotalTime() {
        return getDisplayTime(totalTime);
    }

    public String getDisplayTime(int time) {

        int hours = (int) (time / 3600);
        int remSec = (int) (time % 3600);
        int minutes = (int) (remSec / 60);
        int seconds = (int) (remSec % 60);

        String output = "";
        if(hours < 10) { output += "0"; }
        output +=  String.valueOf(hours) + ":";

        if(minutes < 10) { output += "0"; }
        output += String.valueOf(minutes) + ":";

        if(seconds < 10) { output += "0"; }
        output += String.valueOf(seconds);


        return output;
    }


}
