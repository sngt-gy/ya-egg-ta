package com.example.ya_egg_ta;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sangeight on 12/03/2014.
 * Custom listview adapter
 * Defines custom layout for list view items
 * Also defines an interface that allows
 * to override fucntions, specically operations for button events
 */
public class TimerListAdapter extends BaseAdapter {
    private TimerListAdapterInterface adapterInterface;
    private ArrayList<TimerModel> timers;
    private LayoutInflater inflater;

    public TimerListAdapter(Context context, ArrayList<TimerModel> timers) {
        this.timers = timers;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setAdapterInterface(TimerListAdapterInterface adapterInterface) {
        this.adapterInterface = adapterInterface;
    }

    @Override
    public int getCount() {
        return timers.size();
    }

    @Override
    public Object getItem(int index) {
        return timers.get(index);
    }

    @Override
    public long getItemId(int index) {
        return index;
    }

    @Override
    public View getView(int index, View view, ViewGroup viewGroup) {

        final int itemIndex = index;
        TimerModel timer = timers.get(index);

        if (view == null) {
            view = inflater.inflate(R.layout.listitem_timer, null);
        }

        TextView txtTitle = (TextView) view.findViewById(R.id.itemTitle);
        TextView txtTimer = (TextView) view.findViewById(R.id.itemTimer);
        TextView txtMeta = (TextView) view.findViewById(R.id.itemMeta);
        final Button btnToggle = (Button) view.findViewById(R.id.btnItemTimerToggle);
        Button btnReset = (Button) view.findViewById(R.id.btnItemTimerReset);
        Button btnDelete = (Button) view.findViewById(R.id.btnItemTimerDelete);

        String sTitle = timer.getTitle();
        String sTimer = timer.displayTime() + "";
        String sMeta = timer.displayTotalTime() + "";


        // Display the title and the the timer
        txtTitle.setText(sTitle);
        txtTimer.setText(sTimer);
        txtMeta.setText(sMeta);

        //Enable/Disable toggle button
        if(timer.isCompleted()) {
            btnToggle.setEnabled(false);
        }


        // Set up the listeners for the buttons
        btnToggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapterInterface != null) {
                    // Call the interface function within adapterInferface
                    adapterInterface.toggleTimerEvent(itemIndex);

                    // Change the text on the button depending on the status of the timer
                    String label = view.getResources().getString(R.string.lblTimerResumeButton);
                    if(timers.get(itemIndex).isActive()) {
                        label = view.getResources().getString(R.string.lblTimerPauseButton);
                    }
                    // Set the appropriate text
                    btnToggle.setText(label);
                }
            }
        });

        btnReset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapterInterface != null) {
                    // Call the interface function within adapterInferface
                    adapterInterface.resetTimerEvent(itemIndex);
                }
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapterInterface != null) {
                    // Call the interface function within adapterInferface
                    adapterInterface.deleteTimerEvent(itemIndex);
                }
            }
        });

        return view;

    }




    // Interface to allow overriding functions
    public interface TimerListAdapterInterface {
        public void toggleTimerEvent(int index);
        public void resetTimerEvent(int index);
        public void deleteTimerEvent(int index);
    }
}
