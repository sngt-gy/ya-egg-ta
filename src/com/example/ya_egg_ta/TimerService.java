package com.example.ya_egg_ta;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by sangeight on 27/02/2014.
 * Timer Service that binds with activity
 * Allows for creation and management of timers
 * Spawns runnable thread for each timer using TimerRunner
 */
public class TimerService extends Service {
    private static final String TAG = "YATTA: Timer Service - ";

    private TimerManager timerManager;
    private ArrayList<TimerModel> collection;
    private ArrayList<Thread> threads;
    private IBinder binder = new LocalBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialisations
        timerManager = TimerManager.getInstance();
        collection = timerManager.getCollection();
        threads = timerManager.getThreads();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class LocalBinder extends Binder {
        public TimerService getInstance() {
            return TimerService.this;
        }
    }

    /*
     Creates new timer and adds it to the collection
     Using TimerRunner, starts off the timer in a new thread
     */
    public void newTimer(String title, int time) {
        TimerModel timer = new TimerModel(title, time, true);
        collection.add(timer);

        Thread thread = new Thread(new TimerRunner(timer, TimerService.this));
        threads.add(thread);

        thread.start();
    }

    /*
     Parameter overloading for newTimer
     accepts hours, minutes and seconds
     coverts the total time into seconds
     */
    public void newTimer(String title, int hours, int minutes, int seconds) {
        int time = (hours * 3600) + (minutes * 60) + seconds;
        newTimer(title, time);
    }

    public void toggleTimer(int index) {
        // Check to see if the index is valid
        if(timerManager.checkTimerIndex(index)) {
            TimerModel timer = collection.get(index);
            timer.toggleTimer();
        }
    }

    /*
     Copies the old timer and starts a new thread with it
     using the same index position
     ** Should rename it to restartTimer as it works more like a restart
     */
    public void resetTimer(int index) {
        // Check to see if the index is valid
        if(timerManager.checkTimerIndex(index)) {
            // stop and remove the original timer
            TimerModel oTimer = collection.get(index);
            oTimer.stopTimer();
            collection.remove(index);

            // copy the older timer and insert into the same index
            TimerModel timer = new TimerModel(oTimer.getTitle(), oTimer.getTotalTime(), false);
            collection.add(index, timer);

            // Check to see if the index is valid
            if(timerManager.checkThreadIndex(index)) {
                // Create a new thread for the new timer
                Thread thread = new Thread(new TimerRunner(timer, TimerService.this));

                //Stop the old thread, and replace it with the new thread
                // subsquently, start the thread
                threads.get(index).interrupt();
                threads.set(index, thread);
                thread.start();
            }

            // Reset and start the timer
            collection.get(index).resetTimer();
            collection.get(index).startTimer();
        }
    }

    public void deleteTimer(int index) {
        // Check to see if the index is valid
        if(timerManager.checkTimerIndex(index)) {
            // Stop the timer and remove it from the list
            TimerModel t = collection.get(index);
            t.stopTimer();
            collection.remove(index);
        }
        // Check to see if the index is valid
        if(timerManager.checkThreadIndex(index)) {
            // Stop the thread and remove it.
            threads.get(index).interrupt();
            threads.remove(index);
        }
    }

    /*
      Send notification when timer is complete.
      Called from TimerRunner
     */
    public void notifyCompleted(TimerModel timer) {
        // Check to see if the timer is complete i.e. equals to zero
        if(timer.getTime() == 0) {

            //Get the timer index
            int timerIndex = collection.indexOf(timer);
            int nID = timerIndex; // notification ID

            // Start Building the content for the notification message
            String nTitle = "Yatta! Timer Completed";
            String nContent = "Timer for " + timer.getTitle() + " has been completed."
                    + "\n" + timer.getTime();

            // Action when user clicks the notification
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra("index", timerIndex);

            PendingIntent pendingIntent = PendingIntent.getActivity
                    (TimerService.this, 0, intent, 0);

            // Build the notification
            Notification notification = new Notification.Builder(this)
                    .setContentTitle(nTitle)
                    .setContentText(nContent)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setSmallIcon(android.R.drawable.ic_popup_reminder)
                    .build();

            // Send the notification
            NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nManager.notify("Yatta", nID, notification);


            if(timerManager.checkThreadIndex(timerIndex)) {
                // No longer need the thread to run.
                // Stop the thread
                threads.get(timerIndex).interrupt();
            }
        }
    }

}
