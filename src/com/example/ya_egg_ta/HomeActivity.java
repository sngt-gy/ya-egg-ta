package com.example.ya_egg_ta;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import com.example.ya_egg_ta.TimerListAdapter.*;

public class HomeActivity extends Activity implements TimerListAdapterInterface {
    private static final String TAG = "YATTA: Home Activity - ";

    private TimerManager timerManager;
    private TimerService timerService;
    private TimerListAdapter timersAdapter;
    private ListView listViewTimer;
    private Button btnCreateTimer;
    private boolean isBounded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreated");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Initialisations
        timerManager = TimerManager.getInstance();
        listViewTimer = (ListView) findViewById(R.id.listViewTimer);
        btnCreateTimer = (Button) findViewById(R.id.btnCreateTimer);

        // Start binding to the service
        // and setup listeners
        isBounded = false;
        connectService();
        setListView();
        setUpListeners();
    }

    private void connectService() {
        // bind the timer service to the activity
        Intent intent = new Intent(HomeActivity.this, TimerService.class);
        bindService(intent, conn, BIND_AUTO_CREATE);
    }


    private void setUpListeners() {
        btnCreateTimer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, CreateTimerActivity.class);
                startActivity(i);
                // Make sure we destroy the activity
                finish();
            }
        });

    }

    private void setListView() {
        //Setup the list view with the timers
        timersAdapter = new TimerListAdapter(
                HomeActivity.this, timerManager.getCollection()
        );

        // Set the adapter interface to be able to define
        // operations for events related to buttons internal to the list view
        timersAdapter.setAdapterInterface(this);
        listViewTimer.setAdapter(timersAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
         Allows us to listen to messages for "timer-update"
         so that we can update the timers in the list view
         */
        LocalBroadcastManager.getInstance(HomeActivity.this)
                .registerReceiver(
                        broadcastReceiver,
                        new IntentFilter("timer-update")
                );
        Log.d(TAG, ""+ isBounded);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop listening to broadcast messages
        LocalBroadcastManager.getInstance(HomeActivity.this)
                .unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*
         Make sure the activity is unbounded to the service
         when the activity stops.
         */
        if (isBounded) {
            unbindService(conn);
            isBounded = false;
        }
    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder server) {
            timerService = ((TimerService.LocalBinder) server).getInstance();
            isBounded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            timerService = null;
            isBounded = false;
        }
    };


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateProgress();
        }
    };

    protected void updateProgress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Refresh the list view
                timersAdapter.notifyDataSetInvalidated();
                timersAdapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void toggleTimerEvent(int index) {
        timerService.toggleTimer(index);
        updateProgress();
    }

    @Override
    public void resetTimerEvent(int index) {
        timerService.resetTimer(index);
        updateProgress();
    }

    @Override
    public void deleteTimerEvent(int index) {
        timerService.deleteTimer(index);
        updateProgress();
    }

}
